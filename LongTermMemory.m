% Authors:
%   Petar Milic - pmilic@fesb.hr
%   Josip Bojcic - jbojcic@fesb.hr
% University: MDH, Sweden / University of Split, FESB, Croatia

classdef LongTermMemory
    % Class which represents robot's long term memory.
    %   Long term memory holds robot's beliefs and knowledge.
    %   Beliefs - object of class Beliefs
    %   Knowledge - object of class Knowledge
    
    properties
        Beliefs
        Knowledge
    end
    
    methods
        function obj = LongTermMemory()
            % Constructor of class. Sets knowledge and beliefs.
            
            obj.Knowledge = Knowledge();
            
            
            % Here we set map to some inital values.
            % Robot only knows position of windmill.
            % 0 - occupied. 1 - free.
            %
            %  ind 1 2 3 4 5 6 7 8 9 10
            %   1  1 1 1 1 1 1 1 1 1 1
            %   2  1 1 1 1 1 0 0 1 1 1
            %   3  1 1 1 1 1 0 0 1 1 1
            %   4  1 1 1 1 1 1 1 1 1 1
            %   5  1 1 1 1 1 1 1 1 1 1
            %   6  1 1 1 1 1 1 1 1 1 1
            %   7  1 1 1 1 1 1 1 1 1 1
            %   8  1 1 1 1 1 1 1 1 1 1
            %   9  1 1 1 1 1 1 1 1 1 1
            %  10  1 1 1 1 1 1 1 1 1 1
            
            initialMap = ones(10,10);
            initialMap(2,6) = 0;
            initialMap(3,6) = 0;
            initialMap(2,7) = 0;
            initialMap(3,7) = 0;
            
            % Here we create address book and add windmills position to it.
            addressBook = containers.Map;
            windmillABoundaries = [Position(2, 6, 0) Position(2, 7, 0) Position(3, 6, 0) Position(3, 7, 0)];
            addressBook('windmillA') = MapObject(windmillABoundaries);
            
            %startingPosition = Position(3, 9);
            startingPosition = Position(6, 1);
            
            obj.Beliefs = Beliefs(initialMap, addressBook, startingPosition);
        end

        function returnValue = getKnowledge(obj)
            % Gets knowledge.
            
            returnValue = obj.Knowledge;
        end
        
        function returnValue = getBeliefs(obj)
            % Gets beliefs.
            
            returnValue = obj.Beliefs;
        end
    end
    
end


% Authors:
%   Petar Milic - pmilic@fesb.hr
%   Josip Bojcic - jbojcic@fesb.hr
% University: MDH, Sweden / University of Split, FESB, Croatia

classdef InformationProcessing
    % Class which represents robot's information processing. This component
    % is used mostly for interaction between other components.
    % It is used for:
    %    - updating the robot's beliefs using information from working
    %      memory. 
    %    - processing the robot's decision.
    
    properties
        Memory
        Navigation
    end
    
    methods
        
        function obj = InformationProcessing(memory, navigation)
            % Constructor of the class. Initializes Memory and Navigation
            % so that InformationProcessing can interact with them.
            
            obj.Memory = memory;
            obj.Navigation = navigation;
        end
        
        % ********* MAIN METHODS *****
        
        function updateBeliefs(obj)
            % Updates beliefs of the robot.
            
            obj.updateInRangeFacts();
        end
        
        function processDecision(obj, decision)
            % Processes the robot's decision
            
            % Extracts action and argument from decision. For example if
            % decision is moveNextTo(windmillA) => action is moveNextTo and
            % argument is windmillA.
            [action, argument] = Utilities.parseDecision(decision);
            
            if strcmp(action, 'moveNextTo')
                % Gets the robot's beliefs.
                beliefs = obj.Memory.LTM.getBeliefs();
                
                % Gets the adress book from the beliefs.
                addressBook = beliefs.getAddressBook();
                
                % Processes action.
                obj.moveNextTo(addressBook(argument));
            elseif strcmp(action, 'beginCircling')
                % Gets the robot's beliefs.
                beliefs = obj.Memory.LTM.getBeliefs();
                
                % Gets the adress book from the beliefs.
                addressBook = beliefs.getAddressBook();
                
                % Processes action.
                obj.beginCircle(addressBook(argument).Boundaries, argument);
            elseif strcmp(action, 'circle')
                % Processes action.
                obj.circle(argument);
            elseif strcmp(action, '')
                % If goal state is achieved, action will be empty so we
                % display a message. 
                disp('Command executed.')
            end
        end
        
        % ****************************
        
        
        % ********* HELPER METHODS *****
        
        function updateInRangeFacts(obj)
            % Updates inRange(X) facts.
            
            % Gets robot's beliefs.
            beliefs = obj.Memory.LTM.getBeliefs();
            
            % Gets robot's current position.
            currentPosition = beliefs.getCurrentPosition();
            
            % Gets the address book.
            addressBook = beliefs.getAddressBook();
            
            % Iterates over all objects from address book and check's if
            % robot is next to some object e.g. object a. If it is new fact
            % inRange(a) is added to facts and if it's not the same, fact
            % is removed from the facts.
            for j = addressBook.keys
                i = j{1};
                % We use range 2 to assure that diagonal neighbours are
                % considered.
                if Utilities.isNextToObject(currentPosition, addressBook(i).Boundaries, 2, 0)
                    beliefs.addFact(['inRange(', i, ')']);
                else
                    beliefs.removeFact(['inRange(', i, ')']);
                end
            end
        end
        
        function moveNextTo(obj, mapObject)
            % Processes moveNextTo action. mapObject is the object that
            % robot needs to move next to.
            
            % Gets the robots beliefs.
            beliefs = obj.Memory.LTM.getBeliefs();
            
            % Gets the robot's current position.
            currentPosition = beliefs.getCurrentPosition();
            
            % Gets path from current position to the position next to the
            % given mapObject.
            path = Utilities.getPath(currentPosition, mapObject, beliefs.getMap(), 1);
            
            % We are storing planned path to beliefs without current
            % position just for plotting purpose. 
            pathWithoutCurrentPosition = path;
            
            % If there is planned path, remove current position.
            if(size(path, 2) > 0)
                pathWithoutCurrentPosition = pathWithoutCurrentPosition(2:size(pathWithoutCurrentPosition, 2));
            end
            
            % Store planned path without current position to beliefs.
            beliefs.PlannedPathCoordinates = pathWithoutCurrentPosition;
            
            % Plot path.
            Utilities.plotPath(beliefs.getMap(), pathWithoutCurrentPosition);
            
            % path(1) is always current position so we always send path(2)
            % to navigation.
            if size(path, 2) > 1
                
                % Send move to path(2) position command to actuators here.
                
                % Navigation updates the robot's current position.
                obj.Navigation.update(path(2));
            end
        end
        
        function moveTo(obj, position)
            % Processes moveToAction. Difference from the move to action is
            % that this action moves the robot to exact position given with
            % position argument.
            
            % Gets the robots beliefs.
            beliefs = obj.Memory.LTM.getBeliefs();
            
            % Gets the robot's current position.
            currentPosition = beliefs.getCurrentPosition();
            
            % Gets path from current position to the position.
            path = Utilities.getPath(currentPosition, position, beliefs.getMap(), 0);
            
            % We are storing planned path to beliefs without current
            % position just for plotting purpose. 
            pathWithoutCurrentPosition = path;
            
            % If there is planned path, remove current position.
            if(size(path, 2) > 0)
                pathWithoutCurrentPosition = pathWithoutCurrentPosition(2:size(pathWithoutCurrentPosition, 2));
            end
            
            % Store planned path without current position to beliefs.
            beliefs.PlannedPathCoordinates = pathWithoutCurrentPosition;
            
            % Plot path.
            Utilities.plotPath(beliefs.getMap(), pathWithoutCurrentPosition);
            
            % path(1) is always current position so we always send path(2)
            % to navigation.
            if size(path, 2) > 1
                
                % Send move to path(2) position command to actuators here.
                
                % Navigation updates the robot's current position.
                obj.Navigation.update(path(2));
            end
        end
        
        function beginCircle(obj, boundaries, arg)
            % Processes beginCircle action. Method is called once the robot
            % gets in the range of the object that it has to circle around.
            % It is called only once to find the shortest path to complete
            % circling. The robot can circle around the object of any shape.
            % Object doesn't have to be circular (algorithm can find path
            % around any shaped object such as L shaped or U shaped
            % objects). The algorithm works by finding all free positions
            % that are directly next to the all boundaries on the outside
            % of the object (i.e. O shaped objects have free spaces that
            % are next to boundary but are on the inside) and then sort
            % them in the order in which they should be visited.
            %
            % boundaries - represents an object, not it's surrounding in
            %              real world.
            % arg - name of the object that is being circled
            
            % Gets real map. Map will be used to see surrounding of an
            % object.
            map = obj.Memory.LTM.getBeliefs().getMap();
            
            % First, we find min and max position in an object. That
            % position doesn't have to be on the object, it can be outside
            % (e.g. diamond shaped objects would have min and max outside
            % of them).
            minPosition = boundaries(1);
            maxPosition = boundaries(1);
            for i = boundaries
                if i.X > maxPosition.X
                    maxPosition.X = i.X;
                end
                if i.X < minPosition.X
                    minPosition.X = i.X;
                end
                if i.Y > maxPosition.Y
                    maxPosition.Y = i.Y;
                end
                if i.Y < minPosition.Y
                    minPosition.Y = i.Y;
                end
            end
            
            % Find the difference between min position and the position
            % (2, 2). That difference is used to translate all boundaries
            % closer to the center of coordinate system to make it easier
            % to do the processing and calculations. That is moving of an
            % object from "real world" to the "object world". The "object
            % world" is called workspace here.
            xDifference = 2 - minPosition.X;
            yDifference = 2 - minPosition.Y;
            
            % Moving min and max positions to the "object world".
            minPosition.X = minPosition.X + xDifference;
            minPosition.Y = minPosition.Y + yDifference;
            maxPosition.X = maxPosition.X + xDifference;
            maxPosition.Y = maxPosition.Y + yDifference;
           
            % Creating workspace to fit whole object and have one free row
            % above, under, left and right of the object.
            workspace = ones(maxPosition.X + 1, maxPosition.Y + 1);
            
            for i = 1 : size(boundaries, 2)
                % Moving all boundaries to the "object world".
                boundaries(i).X = boundaries(i).X + xDifference;
                boundaries(i).Y = boundaries(i).Y + yDifference;
                workspace(boundaries(i).X, boundaries(i).Y) = 0;
            end
            
            % Initializing first reachable position from the outside. The
            % position (1, 1) will always be reachable because of the way
            % we constructed the workspace.
            reachablePosition = Position(1, 1);
            
            % Creating array where we will store all the positions we have
            % to visit to circle around the object.
            positionsToVisit = [];
            
            currentPosition = obj.Memory.LTM.getBeliefs().getCurrentPosition();
            
            % Iterate over all the boundaries.
            for i = boundaries
                % For each boundary, find all the free neighbour positions.
                for j = Utilities.getPositionNeighbours(i, workspace)
                    
                    % If that space is reachable from the last reachable
                    % space, that means that the space is also reachable
                    % from the outside which means it should be in the
                    % path. isReachable method uses bfs algorithm to check
                    % if there is a way between reachablePosition and j.
                    if Utilities.isReachable(j, reachablePosition, workspace)
                        
                        % Sets current position as the new reachable
                        % position. We could always use the same space as
                        % reachable, but this is an performance
                        % optimisation. 
                        reachablePosition = j;
                        
                        % Moves the position from the "object world" back to
                        % the "real world" and stores it in
                        % positionsToVisit if it is not already stored and
                        % it is not current position.
                        realCoordinatePosition = j;
                        realCoordinatePosition.X = realCoordinatePosition.X - xDifference;
                        realCoordinatePosition.Y = realCoordinatePosition.Y - yDifference;
                        if map(realCoordinatePosition.X, realCoordinatePosition.Y) == 1 && realCoordinatePosition ~= currentPosition
                            positionsToVisit = Utilities.push(positionsToVisit, realCoordinatePosition, 0);
                        end
                    end 
                end
            end
            
            % After we found all the positions to visit, we need to sort
            % the array to form the shortest possible route.
            positionsToVisit = Utilities.sortByProximity(positionsToVisit, currentPosition, map);
            
            % Stores the path into working memory and add 'circling' fact
            % to the beliefs.
            obj.Memory.WM.setGeneralPurposeData('positionsToVisit', positionsToVisit);
            obj.Memory.LTM.getBeliefs().addFact(['circling(', arg, ')']);
        end
        
        function circle(obj, arg)
            % Processes circle action. arg is the object that is being
            % circled. Circle action is done after beginCircling action is
            % done. beginCircling stores circling path and the circle just
            % visits the positions from the path. 
            
            % Gets the path's positions from the WM. 
            positions = obj.Memory.WM.getGeneralPurposeData('positionsToVisit');
            
            % If there is at least one position in the path, move to that
            % position. If there isn't add circled fact to the robot's
            % beliefs.
            if size(positions, 2) > 0
                obj.moveTo(MapObject(positions(1)));
            else
                beliefs = obj.Memory.LTM.getBeliefs();
                beliefs.removeFact(['circling(', arg, ')']);
                beliefs.addFact(['circled(', arg, ')']);
            end        
        end
        
        % ****************************
    end
    
end


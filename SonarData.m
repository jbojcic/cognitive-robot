% Authors:
%   Petar Milic - pmilic@fesb.hr
%   Josip Bojcic - jbojcic@fesb.hr
% University: MDH, Sweden / University of Split, FESB, Croatia

classdef SonarData < handle
    % Class for sonar data
    %   Sonar provides coordinates of obstacles around the robot's current
    %   position. 
    %   Range - range of sonar. 
    %   Coordinates - list of coordinates of obstacles found in defined
    %   range. Elements of list will be objects of class Position.
    %   RealMap - map which we use only for simualtion purposes, to
    %   simulate sonar detection. For example, during task execution we can
    %   add some obstacle to this map to test bottom up attention. Then
    %   sonar needs to detect obstacle that we added and attention will
    %   update map in beliefs to add detected obstacle.
    
    properties
        Range
        Coordinates
        RealMap
    end
    
    methods
        function obj = SonarData(range)
            % Constructor of class. Sets range and initalizes coordinates
            % list.
            
            obj.Range = range;
            
            % We need to set this dummy Position to make array of objects.
            % Otherwise array of doubles is created and then Position can't
            % be stored in it.
            obj.Coordinates = Position(0,0);
        end
        
        function returnValue = getRange(obj)
            % Gets range of the sonar.
            
            returnValue = obj.Range;
        end
        
        function simulate(obj, currentPosition)
            % Simulates sonar detection. Our sonar provides coordinates of
            % obstacles and free spaces arround robot as if robot is always
            % in the center of the coordinate system. Later attention adds
            % robot's real position coordinates to this coordinates
            % provided by sonar.
            
            % here we store coordinates of obstacles
            coordinatesObstacles = [];
            % here we store coordinates of free spaces
            coordinatesFree = [];
            
            % Here we iterate over concentric circles starting from robots
            % position to detect obstacles and free spaces up to range.
            % This assures that we detect closer obstacles first. That is
            % important to know what sonar can really detect.
            for i = 1 : obj.Range
                for j = -i : i
                    % this processes the line of coordinates in map above robot
                    [coordinatesObstacles, coordinatesFree] = obj.processCoordinates(currentPosition, i, j, coordinatesObstacles, coordinatesFree);
                   
                    % this processes the line of coordinates in map below robot
                    [coordinatesObstacles, coordinatesFree] = obj.processCoordinates(currentPosition, -i, j, coordinatesObstacles, coordinatesFree);
                    
                    % this processes the line of coordinates in map right of robot
                    [coordinatesObstacles, coordinatesFree] = obj.processCoordinates(currentPosition, j, i, coordinatesObstacles, coordinatesFree);
                   
                    % this processes the line of coordinates in map left of robot
                    [coordinatesObstacles, coordinatesFree] = obj.processCoordinates(currentPosition, j, -i, coordinatesObstacles, coordinatesFree);
                end
            end
            
            % We need to set this dummy Position to make array of objects.
            % Otherwise array of doubles is created and then Position can't
            % be stored in it. This reinitializes Coordinates.
            obj.Coordinates = Position(0,0);
            
            % the counter is used for appending new positions to
            % coordinates list
            counter = 1;
            
            % we iterate over obstacles detected by sonar
            for i = 1 : size(coordinatesObstacles, 1)
                
                % convert coordinates back from polar to cartesian
                % coordinate system (pol2cart)
                [x,y] = pol2cart(coordinatesObstacles(i, 1), coordinatesObstacles(i, 2));
                
                % store new position in coordinates
                obj.Coordinates(counter) = Position(round(x), round(y), 0);
                
                % increment counter
                counter = counter + 1;
            end
            
            % we iterate over free spaces detected by sonar
            for i = 1 : size(coordinatesFree, 1)
                
                % convert coordinates back from polar to cartesian
                % coordinate system (pol2cart)
                [x,y] = pol2cart(coordinatesFree(i, 1), coordinatesFree(i, 2));
                
                % create position object
                position = Position(round(x), round(y), 1);
                
                % iterate over coordinates list to check if it already
                % contains this position.
                flag = 0;
                for j = 1 : size(obj.Coordinates, 2)
                    
                    % if coordinates list already contains position set
                    % flag to 1 and break from loop
                    if obj.Coordinates(j) == position
                        flag = 1;
                        break;
                    end
                end
                
                % add position to coordinates list only if it is not
                % already contained in the list (flag is 0)
                if flag == 0
                    obj.Coordinates(counter) = position;
                    
                    % increment counter
                    counter = counter + 1;
                end
            end
        end
             
        function [obs, free] = processCoordinates(obj, currentPosition, i, j, coordinatesObstacles, coordinatesFree)
            % This method is used to process position around the robot. For
            % given position (coordinate) we first transform it to polar
            % coordinate system to make it easier to check if it is visible
            % to the sonar. We do that by checking if there is already
            % detected obstacle on the same angle but with different
            % radius. If there is, that position is not visible by the
            % sonar so it can not be detected. If there is no closer
            % obstacle then we put the position in appropriate list.
            
            % List of obstacles that will be returned. Newly discovered
            % obstacles will be appended to the list.
            obs = coordinatesObstacles;
            
            % List of free spaces that will be returned. Newly discovered
            % free spaces will be appended to the list.
            free = coordinatesFree;
            
            % Checking if the position is inside the map. This may be a
            % limiting factor. Consider allowing map expanding.
            if currentPosition.X + i < 1 || currentPosition.X + i > size(obj.RealMap, 1) || currentPosition.Y + j < 1 || currentPosition.Y + j > size(obj.RealMap, 2)
                return;
            end
            
            % Transformation of position from cartesian to polar coordinate
            % system. We use this to make it easier to detect if the
            % position is visible by the sonar. For two positions on same
            % angle, only the closer one to the robot will be visible to
            % sonar.
            [fi, r] = cart2pol(i, j);
            
            % Checking if the position is inside of sonar's range.
            %if r > obj.Range
            %    return;
            %end
            
            % Check if this position is visible or obscured by some closer
            % obstacle.
            flag = 0;
            for k = 1: size(obs, 1)
                % if there is closer obstacle on same angle, set flag to 1
                if obs(k, 1) == fi
                    flag = 1;
                    break;
                end
            end
            
            % in case the position is visible (no closer obstacle), add
            % position to the appropriate list
            if flag == 0
                % We add the current position coordinates to relative
                % position that was detected by sonar to get absolute
                % position on the map. For absolute position we check value
                % to know if there is obstacle or free space.
                if obj.RealMap(i + currentPosition.X, j + currentPosition.Y) == 0    
                    % append the current angle and radius to the obstacles
                    obs(size(obs, 1) + 1, 1) = fi;
                    obs(size(obs, 1), 2) = r;
                else
                    % append the current angle and radius to free spaces
                    free(size(free, 1) + 1, 1) = fi;
                    free(size(free, 1), 2) = r;
                end
            end
        end
        
        function setRealMap(obj, map)
            % Sets sonar's map.
            
            obj.RealMap = map;          
        end
        
        function initializeRealMap(obj)
            % Here we set a real map for simulation purposes. We use 10x10
            % map. 
            % 0 - occupied. 1 - free.
            %
            %  ind 1 2 3 4 5 6 7 8 9 10
            %   1  1 1 1 0 1 1 1 1 1 1
            %   2  1 1 1 0 1 0 0 1 1 1
            %   3  1 1 1 0 1 0 0 1 1 1
            %   4  1 1 0 0 1 1 1 1 1 0
            %   5  1 1 1 0 0 0 0 1 1 0
            %   6  1 1 1 1 0 1 1 1 1 1
            %   7  1 1 1 1 0 1 1 1 1 1
            %   8  1 1 1 1 0 1 1 1 0 1
            %   9  1 1 1 1 0 1 1 1 1 1
            %  10  1 1 1 1 1 1 1 1 1 1
            
            obj.RealMap = ones(10, 10);
            obj.RealMap(1, 4) = 0;
            obj.RealMap(2, 4) = 0;
            obj.RealMap(3, 4) = 0;
            obj.RealMap(4, 4) = 0;
            obj.RealMap(5, 4) = 0;
            obj.RealMap(5, 5) = 0;
            obj.RealMap(5, 6) = 0;
            obj.RealMap(5, 7) = 0;
            obj.RealMap(4, 10) = 0;
            obj.RealMap(5, 10) = 0;
            obj.RealMap(7, 5) = 0;
            obj.RealMap(8, 5) = 0;
            obj.RealMap(9, 5) = 0;
            obj.RealMap(8, 9) = 0;
            obj.RealMap(2, 6) = 0;
            obj.RealMap(2, 7) = 0;
            obj.RealMap(3, 6) = 0;
            obj.RealMap(3, 7) = 0;
            obj.RealMap(7, 6) = 0;
            obj.RealMap(7, 7) = 0;
            obj.RealMap(6, 7) = 0;
            obj.RealMap(4, 3) = 0;
            obj.RealMap(8, 3) = 0;
        
        end
        
        function updateRealMap(obj, list)
            % Updates sonar's map with values from list. We use this to
            % make real test cases for bottom-up attention. For example
            % when we want to add/remove some obstacle.
            
            for i = list
                obj.RealMap(i.X, i.Y) = i.Value;
            end
        end
    end
    
end


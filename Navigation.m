% Authors:
%   Petar Milic - pmilic@fesb.hr
%   Josip Bojcic - jbojcic@fesb.hr
% University: MDH, Sweden / University of Split, FESB, Croatia

classdef Navigation
    % This class represents navigation and localization of the cogintive
    % robot. The main idea was to use odometry to localize the robot in the
    % map like in the paper "Changyun Wei, Koen V.Hindriks: An Agent-Based
    % Cognitive Robot Architecture (2013)" but for simplicity we just
    % update the position after move without checking for any possible
    % unexpected events like slipping. Later we might add odometry.
    %   Memory - memory of the cognitive robot.
    
    properties
        Memory
    end
    
    methods
        
        function obj = Navigation(memory)
            % Constructor of the class. Initializes Memory.
            
            obj.Memory = memory;
        end
        
        function update(obj, position)
            % Updates the robot's current position. Since for now we just
            % update position after move and without any checking, we are
            % sending position as an argument. In that won't be necessary
            % because odometry will calculate new position.
            
            beliefs = obj.Memory.LTM.getBeliefs();
            beliefs.setCurrentPosition(position);
            
            % Removes position that robot moved to from planned path.
            if(size(beliefs.PlannedPathCoordinates, 2) > 0)
                beliefs.PlannedPathCoordinates = beliefs.PlannedPathCoordinates(2:size(beliefs.PlannedPathCoordinates, 2));
            end
        end
    end
    
end


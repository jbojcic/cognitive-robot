% Authors:
%   Petar Milic - pmilic@fesb.hr
%   Josip Bojcic - jbojcic@fesb.hr
% University: MDH, Sweden / University of Split, FESB, Croatia

classdef CognitiveRobot
    % CognitiveRobot represents whole cognitive robot. Model of the robot
    % is mostly based on following papers: 
    %   1) "Changyun Wei, Koen V.Hindriks: An Agent-Based Cognitive Robot
    %      Architecture (2013)" 
    %   2) "K. Kawamura, W. Dodd, P.Ratanaswasd and R. A. Gutierrez:
    %      Development of a Robot with a Sense of Self (2005)"
    %   3) "Daryl Fougnie: The Relationship between Attention and Working
    %      Memory (2008)"
    % Even though the arhitecture is based on the mentioned papers, there
    % are certain differences.
    %
    % Components of the robot:
    %   * Memory - robot's memory. Mostly based on paper 2)
    %   * Attention - robot's attention. Mostly based on paper 3)
    %   * Sensors - robot's sensors.
    %   * InformationProcessing - component used mostly for interaction
    %     between other components. Multiple arhitecture components from 1)
    %     are integrated into this component with some changes.
    %   * Reasoning - component used for reasoning and decision making.
    %     Idea is based on paper 1) but some changes are made especially in
    %     implementation. 
    %   * Navigation - component used for the robot's localization and
    %     navigation 
    
    properties
        Memory
        Attention
        Sensors
        InformationProcessing
        Reasoning
        Navigation
    end
    
    methods
        function obj = CognitiveRobot()
            % Constructor of a cognitive robot. Creates all main
            % components. 
            
            % Java path is needed to use prolog in matlab. We need to check
            % if the path has already been added because otherwise
            % exception is thrown.
            javaPath = javaclasspath('-dynamic');
            if size(javaPath, 1) == 0
                javaaddpath('C:\Program Files\swipl\lib\jpl.jar');
            end
            
            obj.Sensors = Sensors();
            obj.Memory = Memory();
            obj.Attention = Attention(obj.Memory);
            obj.Navigation = Navigation(obj.Memory);
            obj.InformationProcessing = InformationProcessing(obj.Memory, obj.Navigation);
            obj.Reasoning = Reasoning(obj.Memory);
            
            % initalizes simulation data
            obj.initializeSimulationData();
        end
        
        function initializeSimulationData(obj)
            % Sets all data needed for simulation.
            
            % Sets sonar's map for simulation purposes.
            obj.Sensors.getSonarData().initializeRealMap();
        end
        
        function startCognitiveCycle(obj)
            % Executes one cognitive cycle.
            
            % Simulates all sensors data. 
            obj.Sensors.simulate(obj.Memory.LTM.getBeliefs().getCurrentPosition());
            
            % Get sensory data and store it to STM.
            obj.Memory.STM.getSensoryData(obj.Sensors);
            
            % Process data from STM and store it to WM. 
            obj.Attention.setWM();
            
            % Updates robot's current version of map to add eventual changes
            % discovered by sensors (e.g. sonar detected obastacle that
            % wasn't there before).
            obj.Attention.updateMap();
            
            % Updates robot's beliefs about the environment.
            obj.InformationProcessing.updateBeliefs();
            
            % Updates robot's working memory data connected with action
            % that is being executed.
            obj.Attention.updateWorkingMemory();
            
            % Robot makes decision.
            decision = obj.Reasoning.makeDecision();
            
            % Processes the robot's decision and reacts accordingly.
            obj.InformationProcessing.processDecision(decision);
        end
        
        function start(obj, goalState, displayMaps)
            % Starts the execution of given order.
            % goalState - order is give through this argument. For example
            %             if we want to give an order to the robot to
            %             inspect the windmill A goal stateWould be
            %             {'circled(windmillA)'}
            % displayMaps - 0 or 1. 1 to display maps to vizualize action
            %               execution
            
            % Sets the robot's goal state.
            obj.Memory.WM.setGoalState(goalState);
            flag = 1;
            
            % Start cognitive cycle while the robot doesn't reach goal.
            % When taht happens, goal state will be empty.
            while isempty(obj.Memory.WM.getGoalState()) == 0
                
                if displayMaps
                    % Displays maps for vizualization.
                    Utilities.displayMaps(obj);
                    if flag
                        flag = 0;
                        pause(2.0)
                    else
                        pause(0.5)
                    end
                end
                
                % Starts cognitive cycle.
                obj.startCognitiveCycle();
            end
        end
    end
    
end


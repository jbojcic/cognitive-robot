% Authors:
%   Petar Milic - pmilic@fesb.hr
%   Josip Bojcic - jbojcic@fesb.hr
% University: MDH, Sweden / University of Split, FESB, Croatia

classdef Reasoning
    % Class which represents the robot's reasoning. It is used for planning
    % and decision making which is done based on the robot's knowledge and
    % beliefs.
    % We decided to use strips algorithm for planning and Prolog for it's
    % implementation.
    %   Memory - memory of the cognitive robot. Reasoning needs to have an
    %   access to the memory to be able to make planns and decisions.
    
    properties
        Memory
    end
    
    methods
        function obj = Reasoning(memory)
            % Constructor of the class. Sets Memory and consults prolog
            % file "Strips.pl" which is needed for planning and decision
            % making.
            
            obj.Memory = memory;
        end
        
        function returnValue = makeDecision(obj)
            
            % Makes plan based on current state and goal. If there is no
            % solution we return '';
            x = jpl.Query(['stripsPlan(', obj.Memory.LTM.getBeliefs().getCurrentState(), ', ', obj.Memory.WM.getGoal(), ', Plan).']);
            r = x.allSolutions;
            if isempty(r)
                returnValue = '';
            else
                % Takes just first action from the whole plan (r(1)) and
                % converts it to string.
                
                s = char(r(1).elements.next.toString);
                if strcmp(s, '[]')
                    % If s is [] => there is no solution.
                    returnValue = '';
                else
                    % If there is solution parse string to extract only
                    % action and argument, e.g. moveNextTo(windmillA).
                    s = strsplit(s, ', ');
                    s = s(1);
                    s = regexprep(char(s), '''|(\.)', '');
                    s(1) = '';
                    returnValue = s;
                end
            end
            
            
        end
    end
    
end


% Authors:
%   Petar Milic - pmilic@fesb.hr
%   Josip Bojcic - jbojcic@fesb.hr
% University: MDH, Sweden / University of Split, FESB, Croatia

classdef Position
    % Position in 2D coordinate system. X - x coordinate. Y - y coordinate.
    % Value - occupied (0) or free (1).
    
    properties
        X
        Y
        Value
    end
    
    methods
        function obj = Position(x, y, v)
            % Constructor of class. Sets cordinates and value if provided.  
            obj.X = x;
            obj.Y = y;
            
            % nargin - number of arguments that are sent to constructor. If
            % value wasn't sent nargin will be 2 and value will be set to
            % default value (1).
            if nargin == 3 
                obj.Value = v;
            else
                obj.Value = 1;
            end
        end
        
        function returnValue = eq(a, b)
            % Operator equal (==) for class position. Two objects (a and b)
            % of class Position are equal if they have same x, y
            % coordinates and same value.
            
            returnValue = a.X == b.X && a.Y == b.Y && a.Value == b.Value;
        end
        
        function returnValue = ne(a, b)
            % Operator not equal (~=) for class position. Two objects (a and b)
            % of class Position are not equal if they don't have same x, y
            % coordinates. We don't check value here because we use this
            % for different purpose where value is not important.
            
            returnValue = a.X ~= b.X || a.Y ~= b.Y;
        end
    end
    
end


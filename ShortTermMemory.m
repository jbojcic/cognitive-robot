% Authors:
%   Petar Milic - pmilic@fesb.hr
%   Josip Bojcic - jbojcic@fesb.hr
% University: MDH, Sweden / University of Split, FESB, Croatia

classdef ShortTermMemory < handle
    % Class which represents robot's short term memory.
    %   All data from all sensors are stored to short term memory (STM).
    %   SonarData - used for storing sonar data (object of class Sonar).
    
    properties
        SonarData
        % Add other sensors below
    end
    
    methods
        function returnValue = getSonarData(obj)
            % Gets sonar data.
            
            returnValue = obj.SonarData;
        end
        
        function getSensoryData(obj, sensors)
            % Gets data from all sensors and stores it.
            
            % Gets sonar data and stores it.
            obj.SonarData = sensors.getSonarData();
            
            % Get and store data form other sensors below.
            
        end
    end
    
end


% Authors:
%   Petar Milic - pmilic@fesb.hr
%   Josip Bojcic - jbojcic@fesb.hr
% University: MDH, Sweden / University of Split, FESB, Croatia

classdef Attention < handle
    % Class which represents robot's attention. Attention model is
    % mostly based on the paper  "Daryl Fougnie: The Relationship between
    % Attention and Working Memory (2008)".
    %   Memory - memory of coginitve robot. Memory consists of STM, WM and
    %   LTM. Object of class Memory.
    %   Roles of attention:
    %       - Gets data from STM, process it and store to
    %         working memory. We want to store data to WM before updating 
    %         the  map to respect architecture (other sensory data won�t be 
    %         taken as a whole from STM) and also that data may be used for
    %         something else later.
    %       - Updates the map.
    %       - Updates task related WM data (updateWorkingMemory method).
        
    
    properties
        Memory
    end
    
    methods
        function obj = Attention(memory)
            % Constructor of the class. Initializes Memory.
            
            obj.Memory = memory;
        end
        
        % ********* MAIN METHODS *****
        
        function setWM(obj)
            % Fills WM with processed sensory data. 
            
            % Processing and storing SONAR data.
            obj.processSonarData();
            
            % Plug in OTHER SENSORY DATA below.
        end
        
        function updateWorkingMemory(obj)
            % Updates the robot's task related WM data.
            
            % Updates circling path data.
            obj.updateCirclingPath();
            
            % ADD OTHER METHOD FOR UPDATING TASK RELATED WM DATA BELOW
            
            % Sets the robot's goal state.
            obj.setGoalState();
        end
        
        function updateMap(obj)
            % Updates map to match sonar data. It is assumed that the
            % sonard data is always correct, so all the data is just copied
            % from the sonar into map. Also, if stored map is changed,
            % robot remembers that fact and uses it to decide if the new
            % path planning is needed.
            
            % Initialization of flag and getting of required data.
            mapChangedFlag = 0;
            beliefs = obj.Memory.LTM.getBeliefs();
            map = beliefs.getMap();
            sonarData = obj.Memory.WM.getSonarData();
            
            % Itterate over all sonar data and if the value doesn't match
            % map data, change it and set the flag.
            for i = 1 : size(sonarData.Coordinates, 2)
                position = sonarData.Coordinates(i);
                if map(position.X, position.Y) ~= position.Value
                    mapChangedFlag = 1;
                    map(position.X, position.Y) = position.Value;
                end
            end
            beliefs.setMap(map);
            
            % Update the flag in the working memory.
            if mapChangedFlag
                obj.Memory.WM.setGeneralPurposeData('mapChanged', 1);
            else
                obj.Memory.WM.removeGeneralPurposeData('mapChanged');
            end
        end
        
        % ****************************
        
        
        % ********* HELPER METHODS *****
        
        function processSonarData(obj)
            % Gets SONAR data from STM, processes data by adding robot's
            % real position to coordinates and stores data to WM.
            
            currentPosition = obj.Memory.LTM.getBeliefs().getCurrentPosition();
            sonarData = obj.Memory.STM.getSonarData();
            processedSonarData = SonarData(sonarData.getRange());
            for i = 1:size(sonarData.Coordinates,2) 
                position = Position(sonarData.Coordinates(i).X + currentPosition.X, sonarData.Coordinates(i).Y + currentPosition.Y, sonarData.Coordinates(i).Value);
                processedSonarData.Coordinates(i) = position;
            end
            obj.Memory.WM.setSonarData(processedSonarData);
        end
        
        function updateCirclingPath(obj)
            % Updates circling path used for circling arround objects.
            
            % Gets circling path positions from the WM.
            positions = obj.Memory.WM.getGeneralPurposeData('positionsToVisit');
            
            % Gets the robot's current position.
            currentPosition = obj.Memory.LTM.getBeliefs().getCurrentPosition();
            
            % Iterates over all positions from the path and if robot is
            % currently on that position, removes that position from the
            % path.
            for i = 1 : size(positions, 2)
                if currentPosition == positions(i)
                    positions(i) = '';
                    break;
                end
            end
            
            obj.Memory.WM.setGeneralPurposeData('positionsToVisit', positions);
        end  
       
        function setGoalState(obj)
            % check is goal state reached
            
            facts = obj.Memory.LTM.getBeliefs().getFacts();
            goalState = obj.Memory.WM.getGoalState();
            counter = 0; 
            for i = goalState
               if Utilities.containsForCellElements(facts, i)
                   counter = counter + 1;
               end
            end

            if counter == size(goalState, 2)
                % goal is reached
                obj.Memory.WM.setGoalState({});
            end
        end
        
        % ****************************
    end
    
end


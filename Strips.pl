% Authors:
%   Petar Milic - pmilic@fesb.hr
%   Josip Bojcic - jbojcic@fesb.hr
% University: MDH, Sweden / University of Split, FESB, Croatia

% STRIPS algorithm implemented in prolog. Details about STRIPS you can find 
% on wiki page (http://en.wikipedia.org/wiki/STRIPS).

% Rules
stripsRule(moveNextTo(X), [], [inRange(X)], []).
stripsRule(beginCircling(X), [inRange(X)], [circling(X)], []).
stripsRule(circle(X), [inRange(X), circling(X)], [circled(X)], [circling(X)]).

% Helper methods
preconditionsList(Action, Plist) :- stripsRule(Action, Plist, _, _).
doableAction(Action, State) :- preconditionsList(Action, X), subset(X, State).
subset([], _).
subset([H|T], X) :- member(H, X), subset(T, X).
	
applyRule(State, NewState, Action) :-
	doableAction(Action, State),
	stripsRule(Action, _, Alist, Dlist),
	subtract(State, Dlist, TmpState),
	union(Alist, TmpState, NewState).

%strips planner
stripsPlan(Initial, Goal, Plan) :-
	strips(Goal, [n(Initial,[])], [], RevPlan),
	reverse(RevPlan, Plan).

strips(Goal, [n(CurrentState, Actions)|_], _, Actions) :- 
	subset(Goal, CurrentState).

strips(Goal, [n(CurrentState, Actions)|Remaining], ClosedList, Solution) :- 
	findall(n(S, [A|Actions]), (applyRule(CurrentState, S, A), \+ member(S, [CurrentState|ClosedList])), Es),
	append(Remaining, Es, O),
	strips(Goal, O, [CurrentState|ClosedList], Solution).
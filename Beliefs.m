% Authors:
%   Petar Milic - pmilic@fesb.hr
%   Josip Bojcic - jbojcic@fesb.hr
% University: MDH, Sweden / University of Split, FESB, Croatia

classdef Beliefs < handle
    % Class which represents robot's beliefs.
    %   Beliefs store robot's facts about environment (prolog facts),
    %   current position, current state of map, ...
    %   CurrentPosition - robot's current position in map (2D coordinate
    %   system). CurrentPosition is object of class Position.
    %   Map - robot's current version of map. 2D matrix in which 0 means
    %   that that field is occupied and 1 means that that field is free.
    %   Map in the beliefs can be modified according to newly found
    %   obstacles and it is copied from the knowledge to the beliefs before
    %   each task. 
    %   AddressBook - map of all notable positions in the map. AddressBook
    %   is a dictionary where key is a string representing name of an
    %   object and value is the array of Position-s representing boundary
    %   of an object.
    %   Facts - robot's beliefs about state of environment and it self at
    %   the moment. Facts are used by prolog to make decisions about
    %   robot's next move in Reasoning & Decision making phase. Facts are
    %   represented as a list of strings.
    
    properties
        CurrentPosition
        Map
        AddressBook
        Facts
        
        % Planned path used only for plotting purposes.
        PlannedPathCoordinates
    end
    
    methods
        function obj = Beliefs(map, addressBook, startingPosition)
            % Constructor of class. Sets robot's starting position, map,
            % address book and initializes facts to empty array. 
            
            obj.CurrentPosition = startingPosition;
            obj.Map = map;
            obj.AddressBook = addressBook;
            obj.Facts = {};
        end
        
        function addFact(obj, str)
            % Adds new fact to the robot's beliefs.
            
            % Check for duplicates.
            for i = obj.Facts
                if strcmp(i, str)
                    return
                end
            end
            
            % If there is no same fact in the list already, push the new 
            % fact to the list.
            obj.Facts(size(obj.Facts, 2) + 1) = {str};
        end
        
        function removeFact(obj, str)
            % Removes given fact from the list of facts.
            
            % Look for the fact and if it is found, remove it. Could happen
            % that the robot tries to remove fact which is not in the list
            % already.
            for i = 1: size(obj.Facts, 2)
                if strcmp(str, obj.Facts(i))
                    obj.Facts(i) = '';
                    return;
                end
            end
        end
        
        function returnValue = containsFact(obj, str)
            % Check if the given fact exists in the beliefs.
            
            returnValue = 0;
            for i = 1: size(obj.Facts, 2)
                if strcmp(str, obj.Facts(i))
                    returnValue = 1;
                    return;
                end
            end
        end
        
        function returnValue = getCurrentPosition(obj)
            % Gets robot's current position.
            
            returnValue = obj.CurrentPosition;
        end
        
        function setCurrentPosition(obj, position)
            % Sets robot's current position.
            
            obj.CurrentPosition = position;
        end
        
        function returnValue = getFacts(obj)
            % Gets robot's facts.
            
            returnValue = obj.Facts;
        end
        
        function returnValue = getCurrentState(obj)
            % Gets robot's current idea of it's environment. It is
            % represented as a stringified list of all the facts that the
            % robot believes. e.g. "[inRange(windmillA),
            % circling(windmillA)]"
            
            returnValue = '[';
            temp = {', '};
            
            % Go through all facts and concatenate them into one string.
            for i = 1 : size(obj.Facts, 2)
                returnValue = strcat(returnValue, obj.Facts(i));
                if i ~= size(obj.Facts, 2)
                    returnValue = strcat(returnValue, temp);
                end
            end
            
            returnValue = char(strcat(returnValue, ']'));
        end
        
        function returnValue = getMap(obj)
            % Gets robot's current version of map.
            
            returnValue = obj.Map;
        end
        
        function setMap(obj, map)
            % Sets robot's map.
            
            obj.Map = map;
        end
        
        function returnValue = getAddressBook(obj)
            % Gets address book.
            
            returnValue = obj.AddressBook;
        end
    
        function returnValue = getAllObjectsCoordinates(obj)
            % Gets all coordinates that are stored in the addressBook.
            
            returnValue = [];
            for i = obj.AddressBook.keys
                returnValue = [returnValue, obj.AddressBook(i{1}).Boundaries];
            end
        end
    end
    
end


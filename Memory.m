% Authors:
%   Petar Milic - pmilic@fesb.hr
%   Josip Bojcic - jbojcic@fesb.hr
% University: MDH, Sweden / University of Split, FESB, Croatia

classdef Memory < handle
    % This class represents memory of the cogintive robot. Memory model is
    % mostly based on the paper  "K. Kawamura, W. Dodd, P.Ratanaswasd and
    % R. A. Gutierrez: Development of a Robot with a Sense of Self (2005)"
    %
    %   STM - Short Term Memory. Stores raw sensory data. Object of
    %   class ShortTermMemory.
    %   WM - Working Memory. Stores sensory data processed by
    %   Attention. Object of class WorkingMemory.
    %   LTM - Long Term Memory. Stores knowledge (prolog rules, map) and
    %   beliefs (prolog facts and current state of map). Object of class
    %   LongTermMemory. 
    
    properties
        STM
        WM
        LTM
    end
    
    methods
        function obj = Memory()
            % Constructor of the class. Sets STM, WM and LTM.
            
            obj.STM = ShortTermMemory();
            obj.WM = WorkingMemory();
            obj.LTM = LongTermMemory();
        end
    end
    
end


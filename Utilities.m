% Authors:
%   Petar Milic - pmilic@fesb.hr
%   Josip Bojcic - jbojcic@fesb.hr
% University: MDH, Sweden / University of Split, FESB, Croatia

classdef Utilities
    % Static class which contains all different kind of utility methods.

    properties
    end
    
    
    methods(Static)
        
        function plotPath(map, path)
            for i = 1 : size(path, 1)
                for j = 1 : size(path, 2)
                    rectangle('Position',[path(i,j).Y (size(map, 1) + 1 - path(i,j).X) 1 1], 'FaceColor','y')
                end
            end
        end
        
        function plotMap(map, cr, plotPath)
            % Plots map. In this model the map is divided into discrete
            % fields (e.g. field (1,1), (2,3), ...) sarting from field
            % (1,1). Every field can have value 0 (obstacle) or 1 (free
            % space). So map is actually 2d matrix of 0 or 1 values. In the
            % one moment the robot can be only on one field. 
            % 
            % When plotting map the obstacles are represented with black
            % squares, except for the fields occupied by objects which are
            % represented with blue squares. The robot is represented with
            % a red circle and the robot's sonar range with a green
            % circular.
            %   map - map for plotting
            %   cr - cognitive robot. This argument is optional.
            
            % Sets plot's axis.
            axis([1 (size(map, 1) + 1) 1 (size(map, 2) + 1)])
            
            % Hides ticks from axis.
            set(gca,'XTick',[])
            set(gca,'YTick',[])
            
            % Gets beliefs.
            beliefs = cr.Memory.LTM.getBeliefs();
            
            % Gets the coordinates for all objects.
            objectsCoordinates = beliefs.getAllObjectsCoordinates();
            
            % Take planned path from beliefs.
            path = beliefs.PlannedPathCoordinates;
            
            % Iterates over map.
            for i = 1 : size(map, 1)
                for j = 1 : size(map, 2)
                    % If there is obstacle on i,j position in the map, draw
                    % the rectangle.

                    if map(i, j) == 0
                        % If the obstacle is the object, draw blue square,
                        % otherwise draw black square.
                        
                        if Utilities.contains(objectsCoordinates, Position(i, j, 0))
                            rectangle('Position',[j (size(map, 1) + 1 - i) 1 1], 'FaceColor','b', 'EdgeColor', 'b')
                        else
                            rectangle('Position',[j (size(map, 1) + 1 - i) 1 1], 'FaceColor','k')
                        end
                    elseif Utilities.contains(path, Position(i, j, 1)) && plotPath 
                        % If this position is in the planned path and
                        % plotPlan flaf is true, mark it with yellow color. 
                        rectangle('Position',[j (size(map, 1) + 1 - i) 1 1], 'FaceColor','y')
                    end
                end
            end
            
            % nargin is number of arguments that are sent to method. If cr
            % argument wasn't sent nargin will be 1 so this part won't be
            % executed => robot won't be displayed on the plot.
            if nargin > 1
                % Gets the robot's current position.
                robotPosition = cr.Memory.LTM.getBeliefs().getCurrentPosition();
                % Gets a sonar's range.
                range = cr.Sensors.Sonar.getRange();
                % Draws the sonar's range on the map as a green circular.
                rectangle('Position', [robotPosition.Y - range (size(map, 1) + 1 - robotPosition.X - range) (range*2 + 1) (range*2 + 1)], 'Curvature', [1 1], 'EdgeColor', 'g')
                % Draws the robot on the map as a red circle.
                rectangle('Position', [robotPosition.Y (size(map, 1) + 1 - robotPosition.X) 1 1], 'Curvature', [1 1], 'FaceColor','r')
            end
        end
        
        function displayMaps(cognitiveRobot)
            % Displays the real world map and map which robot sees as two
            % subplots.
            % cognitiveRobot - cognitve robot whose maps will be displayed.
            
            clf
            subplot(2,1,1)
            title('Real map')
            Utilities.plotMap(cognitiveRobot.Sensors.Sonar.RealMap, cognitiveRobot, 0)
            subplot(2,1,2)
            title('Map which robot sees')
            Utilities.plotMap(cognitiveRobot.Memory.LTM.getBeliefs().getMap(), cognitiveRobot, 1)
        end
        
        function returnValue = getDistance2D(p1, p2)
            % Gets euclidean distance between two positions (wiki euclidean
            % distance - http://en.wikipedia.org/wiki/Euclidean_distance).
            %   p1 - position 1.
            %   p2 - position 2.
            returnValue = sqrt((p1.X-p2.X)^2 + (p1.Y-p2.Y)^2);
        end
        
        function returnValue = getNodeNeighbours(node, map, destination)
            % Gets all neigbour nodes.
            %   node - current node.
            %   map - map from where we take the neighbour nodes.
            %   destination - destination node. Needed to set heuristic
            %   value for neighbour nodes.
            
            returnValue = [];
            path = node.Path;
            
            % Adds current node to path.
            path = Utilities.push(path, node);
            
            % Adds the first node from the right to the neighbour list, if
            % position doesn't go out of the map and there is no obstacle 
            % on that position. 
            position = Position(node.Position.X + 1, node.Position.Y);
            if position.X <= size(map, 1) && map(position.X, position.Y) == 1
                returnValue = Utilities.push(returnValue, Node(position, path, destination));
            end
            
            % Adds the first node from the left to the neighbour list, if
            % position doesn't go out of the map and there is no obstacle
            % on that position. 
            position = Position(node.Position.X - 1, node.Position.Y);
            if position.X > 0 && map(position.X, position.Y) == 1
                returnValue = Utilities.push(returnValue, Node(position, path, destination));
            end
            
            % Adds the first upper node to the neighbour list, if position
            % doesn't go out of the map and there is no obstacle on that 
            % position. 
            position = Position(node.Position.X, node.Position.Y + 1);
            if position.Y <= size(map, 2) && map(position.X, position.Y) == 1
                returnValue = Utilities.push(returnValue, Node(position, path, destination));
            end
            
            % Adds the first lower node to the neighbour list, if position
            % doesn't go out of the map and there is no obstacle on that
            % position. 
            position = Position(node.Position.X, node.Position.Y - 1);
            if position.Y > 0 && map(position.X, position.Y) == 1
                returnValue = Utilities.push(returnValue, Node(position, path, destination));
            end
            
        end
        
        function returnValue = getPositionNeighbours(position, map)
            % Gets all neigbour positions.
            %   node - current node.
            %   map - map from where we take the neighbour nodes.
            
            returnValue = [];
            
            % Adds the first position from the right to the neighbour list,
            % if position doesn't go out of the map and there is no
            % obstacle on that position.  
            if position.X + 1 <= size(map, 1) && map(position.X + 1, position.Y) == 1
                returnValue = Utilities.push(returnValue, Position(position.X + 1, position.Y));
            end
            
            % Adds the first position from the left to the neighbour list,
            % if position doesn't go out of the map and there is no
            % obstacle on that position.  
            if position.X - 1 > 0 && map(position.X - 1, position.Y) == 1
                returnValue = Utilities.push(returnValue, Position(position.X - 1, position.Y));
            end
            
            % Adds the first upper position to the neighbour list, if
            % position doesn't go out of the map and there is no obstacle
            % on that position.   
            if position.Y + 1 <= size(map, 2) && map(position.X, position.Y + 1) == 1
                returnValue = Utilities.push(returnValue, Position(position.X, position.Y + 1));
            end
            
            % Adds the first lower position to the neighbour list, if
            % position doesn't go out of the map and there is no obstacle
            % on that position. 
            if position.Y - 1 > 0 && map(position.X, position.Y - 1) == 1
                returnValue = Utilities.push(returnValue, Position(position.X, position.Y - 1));
            end
            
        end
            
        function returnValue = contains(list, element)
            % Checks if list contains element. Returns 1 if element is
            % contained and 0 otherwise. It WORKS ONLY if the TYPE of the
            % element HAS an EQ operator DEFINED. 
            %   list - list/array of elements.
            %   element - element we are checking.
            returnValue = 0;
            for i = list
                if i == element
                    returnValue = 1;
                    break;
                end
            end
        end
        
        function returnValue = containsForCellElements(list, element)
            % Checks if list of cell objects contains cell element. Cell
            % elements are strings. Returns 1 if element is contained and 0
            % otherwise.
            %   list - list/array of elements.
            %   element - element we are checking.
            % We have to have separate contains method for cell elements
            % because cell elements don't have eq operator so we use i{1}
            % and element{1} to compare content of cell instead of the
            % cell.
            
            returnValue = 0;
            for i = list
                if strcmp(i{1}, element{1})
                    returnValue = 1;
                    break;
                end
            end
        end
        
        function [returnValue, returnIndex] = popBestSolution(nodes)
            % Returns node with best solution and it's index from the list.
            % Node with best solution is node with min sum of the path size
            % and heuristic value.
            %   nodes - list of nodes.
            
            returnValue = nodes(1);
            returnIndex = 1;
            for i = 2 : size(nodes, 2)
                if size(nodes(i).Path, 2) + nodes(i).HeuristicValue < size(returnValue.Path, 2) + returnValue.HeuristicValue
                    returnValue = nodes(i);
                    returnIndex = i;
                end
            end
        end
        
        function returnValue = getElementIndex(node, nodes)
            % Returns index of the elment in the list.
            %   node - node whose index will be returned.
            %   nodes - list of nodes.
            
            returnValue = 0;
            for i = 1 : size(nodes, 2)
                if nodes(i) == node
                    returnValue = i;
                    break;
                end
            end
        end
        
        function returnValue = isNextToObject(node, nodes, range, includeRange)
            % Returns 1 if the node is next to at least one of the nodes
            % from the nodes list and 0 otherwise. Node is next to other
            % node if euclidean distance between their positions is
            % less/less or equal than the given range.
            %   node - node who's distance we are checking. It can be
            %   object of the class Node or Position.
            %   nodes - list of nodes.  It can be list of an objects of the
            %   class Node or Position. 
            %   range - defines proximity of nodes. If distance between two
            %   nodes is less/less or equal to range => nodes are next to
            %   each other.
            
            returnValue = 0;
            
            % Iterates over nodes.
            for i = 1 : size(nodes, 2)
                
                % If node is an object of class Node, it will have Position
                % property so position1 will be node.Position and position2
                % will be nodes(i).Position. If node is an object of the
                % class Position, it won't have Position property so
                % exception will be thrown and position1 will be set to
                % node and position2 to nodes(i).
                try
                    position1 = node.Position;
                    position2 = nodes(i).Position;
                catch
                    position1 = node;
                    position2 = nodes(i);
                end
                
                if includeRange
                    % Checks distances between positions with range
                    % included.
                    if Utilities.getDistance2D(position1, position2) <= range
                        returnValue = 1;
                        break;
                    end
                else
                    % Checks distances between positions with range
                    % excluded.
                    if Utilities.getDistance2D(position1, position2) < range
                        returnValue = 1;
                        break;
                    end
                end
            end
            
        end
        
        function returnValue = aStar(destinationNodes, destinationCenter, openedNodes, closedNodes, map, range)
            % Recursive implementation of an A* algorithm. Returns the
            % shortest path to the destination.
            %   destinationNodes - node(s) that robot has to come next to.
            %   destinationCenter - center (arithmetic average) of.
            %   destinationNodes - used by algorithm to calculate heuristic
            %   value to know the direction in which robot should go.
            %   Heuristic value is euclidian distance.
            %   openNodes - list of open nodes that the algorithm has to
            %   check
            %   closedNodes - list of closed nodes that the algorithm has
            %   checked already.
            %   map - the map(graph) used by algorithm.
            %   range - the range which is used to determine if the robot
            %   is close enough to the destination or not.
            % Details of the A* algorithm can be seen on wiki page
            % (http://en.wikipedia.org/wiki/A*_search_algorithm). 
            
            % If there are no more nodes to be opened, return empty path.
            if size(openedNodes, 2) == 0
                returnValue = [];
                return;
            end
            
            % Gets currently closest node from the openedNodes list. This
            % is based on the lenght of current path to the node summed
            % with the expected length to the destination (heuristic
            % value).
            [node, index] = Utilities.popBestSolution(openedNodes);
            
            % Delets node from openedNodes list and adds it to the
            % closedNodes list.
            openedNodes(index) = '';
            Utilities.push(closedNodes, node);
            
            % Returns node if it is close enough to the destination.
            if Utilities.isNextToObject(node, destinationNodes, range, 1)
                returnValue = node;
            else
                % Gets all the neighbours of the node
                for neighbourNode = Utilities.getNodeNeighbours(node, map, destinationCenter)
                    % If the node has already been visited (it is in
                    % closedNodes list) then skip it
                    if Utilities.contains(closedNodes, neighbourNode)
                        continue;
                    end
                    
                    % Otherwise, find if it is already in opened nodes 
                    index = Utilities.getElementIndex(neighbourNode, openedNodes);
                    if index > 0
                        % If it is, check if the new one has shorter path
                        % than the previous one and if it has then swap
                        % them, otherwise skip it
                        if size(neighbourNode.Path, 2) < size(openedNodes(index).Path, 2)
                            openedNodes(index) = neighbourNode;
                        end
                    else
                        % If it is not in the openedNodes, add it
                        openedNodes(size(openedNodes, 2) + 1) = neighbourNode;
                    end
                end
                
                % Recursive call
                returnValue = Utilities.aStar(destinationNodes, destinationCenter, openedNodes, closedNodes, map, range);
            end
            
        end
        
        function returnValue = getPath(currentPosition, destination, map, range)
            % Gets the best path between currentPosition and
            % the destination. Internaly it uses the A* algoritham.
            %   currentPosition - object of the class Position, represents
            %   the start for the path planning.
            %   destination - object of the class MapObject, represents the
            %   end for path planning.
            %   map - a map which is used by A*
            %   range - range which is used to determine if the node is
            %   close enough to destination or not.
            % The method returns an array of objects of the class Position
            % or an empty array if there is no solution.
            
            % Initially, openedNodes is just starting Node which is formed
            % from currentPosition.
            openedNodes = Node(currentPosition, [], destination.Center);
            
            % Forms the destinationNodes list from Boundaries.
            destinationNodes = [];
            for i = 1 : size(destination.Boundaries, 2)
                destinationNodes = Utilities.push(destinationNodes, Node(destination.Boundaries(i), [], destination.Center));
            end
            
            % Calls A* method.
            solution = Utilities.aStar(destinationNodes, destination.Center, openedNodes, [], map, range);
            returnValue = [];
            
            % Checks if solution exists by looking at the size of path.
            if size(solution, 1) > 0
                % If it exists, it is an array of objects of class Node, so 
                % we transform them back to class Position
                for i = 1 : size(solution.Path, 2)
                    returnValue = Utilities.push(returnValue, solution.Path(i).Position);
                end
                returnValue = Utilities.push(returnValue, solution.Position);
            end
        end
        
        function returnValue = isReachable(position, reachablePosition, map)
            % Finds if some position is reachable from another
            % position in the given map. This is done by finding at least
            % one path between two positions. If there is no path, the
            % position is not reachable. Internally, this algorithm uses
            % BFS. 
            %   position - object of the class Position, represents
            %   starting position from which the algorithm does the
            %   searching 
            %   reachablePosition - object of the class Position,
            %   represents the target position for the path finding
            %   map - a map used by the BFS
            % The method returns 1 if the path exists or 0 if there is no 
            % path.
            
            % Initially, openedNodes is just starting Node which is formed
            % from position.
            openedNodes = Node(position, [], reachablePosition);
            
            % destinationNode is formed from reachablePosition.
            destinationNode = Node(reachablePosition, [], reachablePosition);
            
            % Calls the BFS method.
            solution = Utilities.bfs(openedNodes, [], destinationNode, map);
            
            % If the solution exists, returns 1, otherwise returns 0.
            returnValue = 0;
            if size(solution, 1) > 0
                returnValue = 1;
            end
        end
        
        function returnValue = bfs(openedNodes, closedNodes, destinationNode, map)
            % Recursive implementation of a BFS algorithm. Return value is
            % a path (first one that occures) to the destination, or empty
            % array if there is no path. 
            %   openedNodes - list of the opened nodes that the algorithm
            %   has to check .
            %   closedNodes - list of the closed nodes that the algorithm
            %   has checked already.
            %   destinationNode - node that robot has to come to.
            %   map - the map used by the BFS
            % Details of the BFS algorithm can be seen on wiki page
            % (http://en.wikipedia.org/wiki/Breadth-first_search). 
            
            % If there are no more nodes to be opened, return empty path.
            if size(openedNodes, 2) == 0
                returnValue = [];
                return;
            end
            
            % Pops the first node from the queue.
            node = openedNodes(1);
            openedNodes(1) = '';
            
            % Pushes the node to the closedNodes list.
            closedNodes = Utilities.push(closedNodes, node);
            
            % If the current node is destination node, returns it.
            if node == destinationNode
                returnValue = node;
            else
                % Otherwise, gets all neighbours for the node.
                for neighbourNode = Utilities.getNodeNeighbours(node, map, destinationNode.Position)
                    % For each neighbour, checks if it was visited already
                    % (is it contained in closedNodes) and if it is, skips
                    % it .
                    if Utilities.contains(closedNodes, neighbourNode)
                        continue;
                    end
                    
                    % If it wasn't visited, pushes it to the openedNodes
                    % queue.
                    openedNodes = Utilities.push(openedNodes, neighbourNode);
                end
                
                % Recursive call.
                returnValue = Utilities.bfs(openedNodes, closedNodes, destinationNode, map);
            end
            
        end
        
        function returnValue = push(array, element, allowDuplicates)
            % Pushes element to the end of the array.
            %   array - array to push element to.
            %   element - object to be pushed.
            %   allowDuplicates - flag to allow duplicates in the array.
            %   Default value is true. Optional argument.
            
            % If allowDuplicates is not sent, sets it to true.
            if nargin < 3 
                allowDuplicates = 1;
            end
            
            % If the array doesn't exist yet, sets it to the element. This
            % is needed because Matlab cannot push object of the custom
            % class to the empty array.
            if size(array, 2) == 0
                array = element;
            else
                % If the duplicates are allowed or if the element isn't in
                % the array already, adds it.
                if allowDuplicates || Utilities.contains(array, element) == 0;
                    array(size(array, 2) + 1) = element;
                end
            end
            
            returnValue = array;
        end
        
        function returnValue = sortByProximity(array, currentPosition, map)
            % Sorts an array to make the shortest path needed to visit each
            % position once, starting from the currentPosition. Returns
            % sorted array of the positions.
            %   array - an array with the positions to sort.
            %   currentPosition - the starting position for the algorithm.
            %   map - a map used by the algorithm.
            
            returnValue = [];
            
            % Iterates while there are still more positions in array left
            % to be visited.
            while size(array, 2) > 0
                % Finds the index of the closest position from the array to
                % the currentPosition.
                
                % Initializes minIndex to first element, and finds
                % it's distance to the currentPosition.
                minIndex = 1;
                minDistance = size(Utilities.getPath(currentPosition, MapObject(array(minIndex)), map, 0), 2);
                
                % Iterates over the other positions, and if the distance
                % to the currentPosition is smaller than currently saved 
                % one, takes it as the currently closest next position.
                for i = 2 : size(array, 2)
                    currentDistance = size(Utilities.getPath(currentPosition, MapObject(array(i)), map, 0), 2);
                    if  currentDistance < minDistance
                        minIndex = i;
                        minDistance = currentDistance;
                    end
                end
                
                % When the closest next position to visit is found, pushes
                % it to the solution and removes it from the array.
                returnValue = Utilities.push(returnValue, array(minIndex));
                currentPosition = array(minIndex);
                array(minIndex) = '';
            end
        end
        
        function [action, argument] = parseDecision(decision)
            % Parses the robot's decision. "decision" argument is string,
            % e.g. moveNextTo(windmillA) so function will parse it and set
            % action to moveNextTo and  argument to windmillA.
            
            if strcmp(decision, '')
                action = '';
                argument = '';
            else
                action = strsplit(decision, '(');
                argument = action(2);
                action = action(1);
                action = action{1};
                argument = argument{1};
                argument = strsplit(argument, ')');
                argument = argument(1);
                argument = argument{1};
            end
        end
    end
end

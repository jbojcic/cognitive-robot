% Authors:
%   Petar Milic - pmilic@fesb.hr
%   Josip Bojcic - jbojcic@fesb.hr
% University: MDH, Sweden / University of Split, FESB, Croatia

classdef MapObject
    % Class for a map object
    %   Map object is used to define some object on the map to let robot
    %   know that all those "obstacles" in the map belong to the same
    %   object. 
    %   Boundaries - List of objects of class Position representing all the
    %   positions the object occupies in the map.
    %   Center - We calculate the center of the object based on the
    %   boundaries it has. This center is used to determine the direction
    %   in which the object is. Center is object of class Position.
    
    properties
        Boundaries
        Center
    end
    
    methods
        function obj = MapObject(boundaries)
            % Constructor of a map object. It recieves all the boundaries
            % as an array of Position-s. The boundaries are stored and then
            % the center is calculated based on those boundaries as an
            % arithmetic average of all the boundaries.
            
            obj.Boundaries = boundaries;
            obj.Center = Position(0, 0);
            
            for i = 1 : size(boundaries, 2)
                obj.Center.X = obj.Center.X + boundaries(i).X;
                obj.Center.Y = obj.Center.Y + boundaries(i).Y;
            end
            
            obj.Center.X = obj.Center.X / size(boundaries, 2);
            obj.Center.Y = obj.Center.Y / size(boundaries, 2);
        end
        
        function returnValue = getBoundaries(obj)
            % Geter for boundaries.
            returnValue = obj.Boundaries;
        end
        
        function returnValue = getCenter(obj)
            % Geter for center of the object.
            returnValue = obj.Center;
        end
    end
    
end


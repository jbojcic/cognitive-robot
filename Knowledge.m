% Authors:
%   Petar Milic - pmilic@fesb.hr
%   Josip Bojcic - jbojcic@fesb.hr
% University: MDH, Sweden / University of Split, FESB, Croatia

classdef Knowledge
    % Class which represents robot's knowledge.
    %    Stores robot's knowledge that cannot be changed. In current
    %    implementation knowledge is basically prolog file.
    
    properties
        % Intention was to have prolog facts (STRIPS rules) used for STRIPS
        % planning stored in one property in Knowledge but dynamic
        % consulting of facts in prolog wasn't working so we just put it in
        % file. So knowledge is basically prolog file.
    end
    
    methods
        function obj = Knowledge()
            % Constructor of the class. Consults prolog file.
            
            % Consults strips algorithm prolog file and throws error if
            % file hasn't been found. 
            x = jpl.Query('consult(''Strips.pl'')');
            flag = x.hasSolution;
            if flag == 0
               msgID = 'Knowledge:BadFilePath';
               msg = 'Unable to find strips prolog file.';
               throw(MException(msgID,msg)); 
            end
        end
        
    end
    
end


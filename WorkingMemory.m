% Authors:
%   Petar Milic - pmilic@fesb.hr
%   Josip Bojcic - jbojcic@fesb.hr
% University: MDH, Sweden / University of Split, FESB, Croatia

classdef WorkingMemory < handle
    % Class which represents robot's working memory.
    %   Working memory will store sensory data processed by attention.
    %   SonarData - holds processed sonar data (data got from sonar summed
    %   with current position)
    %   GoalState - stores the state which the robot strives to. It is used
    %   by strips algorithm in reasoning & decision making phase.
    %   GeneralPupropseData - a dictionary of all kinds of data used by the
    %   robot. Currently it is used for storing path for circling around an
    %   object.
    
    properties
        SonarData
        % Add other sensors data below.
        
        GoalState
        GeneralPurposeData
    end
    
    methods
        function obj = WorkingMemory()
            % Constructor for the class.
            
            obj.GeneralPurposeData = containers.Map;
        end
        
        function setGeneralPurposeData(obj, key, data)
            % Method used to add more data to the GeneralPurposeData dict.
            
            obj.GeneralPurposeData(key) = data;
        end
        
        function returnValue = getGeneralPurposeData(obj, key)
            % Method used to get some data by the key provided.
            
            if isKey(obj.GeneralPurposeData, key)
                returnValue = obj.GeneralPurposeData(key);
            else
                returnValue = [];
            end
        end
        
        function removeGeneralPurposeData(obj, key)
            % Method used to delete some entry from the GeneralPurposeData.
            
            obj.GeneralPurposeData(key) = [];
        end
        
        function setSonarData(obj, sonarData)
            % Sets sonar data.
            
            obj.SonarData = sonarData;
        end
        
        function setGoalState(obj, goalState)
            % Sets goal state.
            
            obj.GoalState = goalState;
        end
        
        function returnValue = getSonarData(obj)
            % Gets sonar data.
            
            returnValue = obj.SonarData;
        end
        
        function returnValue = getGoalState(obj)
            % Gets goal state.
            
            returnValue = obj.GoalState;
        end
        
        function returnValue = getGoal(obj)
            % Gets goal state in form of stringified array of the goals.
            
            returnValue = '[';
            temp = {', '};
            
            for i = 1 : size(obj.GoalState, 2)
                returnValue = strcat(returnValue, obj.GoalState(i));
                if i ~= size(obj.GoalState, 2)
                    returnValue = strcat(returnValue, temp);
                end
            end
            
            returnValue = char(strcat(returnValue, ']'));
        end
    end
    
end


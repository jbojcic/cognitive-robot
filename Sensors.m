% Authors:
%   Petar Milic - pmilic@fesb.hr
%   Josip Bojcic - jbojcic@fesb.hr
% University: MDH, Sweden / University of Split, FESB, Croatia

classdef Sensors
    % Sensors represents all robot sensors. Video, sonar, audio, ...
    %   For now we'll use just sonar.
    %   Sonar - sonar sensory data. Object of class SonarData
    
    properties
        Sonar
    end
    
    methods
        function obj = Sensors()
            % Constructor of sensors. Initializes all used sensors.
            
            % Sets sonar.
            obj.Sonar = SonarData(2);
        end
        
        function simulate(obj, currentPosition)
            % Simulates sensor detection.
            
            % Simualtes sonar detection.
            obj.Sonar.simulate(currentPosition);
        end
        
        function returnValue = getSonarData(obj)
            % Gets sonar.
            
            returnValue = obj.Sonar;
        end
       
    end
    
end


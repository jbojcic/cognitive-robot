% Authors:
%   Petar Milic - pmilic@fesb.hr
%   Josip Bojcic - jbojcic@fesb.hr
% University: MDH, Sweden / University of Split, FESB, Croatia

classdef Node
    % Class for Node used in the graph algorithms.
    %   Node is used in the BFS and A* helper algorithms. 
    %   Position - object of class Position. It stores the position that
    %   the node represents.
    %   Path - list of objects of class Node. Used to determine the path
    %   to the Position of the node.
    %   HeuristicValue - a floating point numer. Represents approximate
    %   distance between the node and the destination of path finding
    %   algorithm. It is calculated as euclidean distance between the
    %   position and the destination.
    
    properties
        Position
        Path
        HeuristicValue
    end
    
    methods
        function obj = Node(position, path, destination)
            % Constructor of the class. 
            obj.Position = position;
            obj.Path = path;
            % Destination is not stored in the object, but only used to
            % find the heuristic value of the node.
            obj.HeuristicValue = Utilities.getDistance2D(position, destination);
        end
        
        function returnValue = eq(a, b)
            % Operator equal (==) for the class Node. Two objects (a and b)
            % of class Node are equal if they have the same positions
            % stored inside of them, regardless of the path they used to 
            % get there.
            returnValue = a.Position == b.Position;
        end
    end
    
end

